import { initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from 'firebase/auth';
import './index.scss';
import './auth.scss';
import { async } from '@firebase/util';

const app = initializeApp({
    apiKey: "AIzaSyCATOyH-qdoMxWsSVklA329b4n49zrUG4k",
    authDomain: "crwn-clothing-db-8793f.firebaseapp.com",
    projectId: "crwn-clothing-db-8793f",
    storageBucket: "crwn-clothing-db-8793f.appspot.com",
    messagingSenderId: "357488234667",
    appId: "1:357488234667:web:9d411982ba740ad0dd27ee"
});

const auth = getAuth();

const btn_container = document.getElementById('response_container');
const login_container = document.getElementById('login_container');

const singin_form = document.getElementById('singin_form');
const email_field = document.getElementById('email_field');
const password_field = document.getElementById('password_field');
const btn_authin = document.getElementById('btn-authin');
const btn_authout = document.getElementById('btn-authout');
const error_msg = document.getElementById('error-msg');

const singup_form = document.getElementById('singup_form');
const singup_container= document.getElementById('singup_container');
const btn_signup = document.getElementById('btn-signup');
const error_msg_signup = document.getElementById('error-msg_signup');
const signup_display_name = document.getElementById('signup_display_name');
const signup_email_field = document.getElementById('signup_email_field');
const signup_password_field = document.getElementById('signup_password_field');

onAuthStateChanged(auth, function (user) {
    if (user) {
        let guest = user.isAnonymous;        
        btn_authin.classList.add('d-none');
        btn_authout.classList.remove('d-none');
        btn_container.insertAdjacentHTML('beforebegin', `<h3 class="user_title">${user.uid}${guest ? ' (Guest)' : `(${user.displayName})`}</h3>`);
        btn_container.textContent = JSON.stringify(user);
        btn_container.classList.remove('d-none');
        singin_form.classList.add('d-none');
        singup_container.classList.add('d-none');
    } else {
        btn_container.classList.add('d-none');
        singin_form.classList.remove('d-none');
        btn_authin.classList.remove('d-none'); 
        singup_container.classList.remove('d-none');
        btn_authout.classList.add('d-none');       
        btn_container.textContent = '';
        let ele = document.getElementsByClassName('user_title');
        while (ele.length > 0)
            ele[0].remove();
    }
});

async function auth_function() {
    error_msg.textContent = '';    
    btn_authin.classList.add('btn-disabled');
    btn_authout.classList.add('btn-disabled');
    try {
        if (!auth.currentUser) {
            await signInWithEmailAndPassword(auth, email_field.value, password_field.value);
        } else {
            await signOut(auth);
        }
    } catch (e) {
        switch (e.code) {
            case 'auth/user-not-found':
                error_msg.textContent = 'User not Found';
                break;
            case 'auth/wrong-password':
                error_msg.textContent = 'Wrong password';
                break;
            default:
                error_msg.textContent = 'Something went wrong';
                console.log(e.code);
                console.log(e);
                break;
        }

    } finally {
        btn_authin.classList.remove('btn-disabled');
        btn_authout.classList.remove('btn-disabled');
    }

}

singin_form.addEventListener('submit', async function (e) {    
    e.preventDefault();
    await auth_function();
    return;  
});

btn_authout.onclick = async function(){
    email_field.textContent = '';
    password_field.textContent = '';
    await auth_function();
}



async function signup_function() {
    error_msg.textContent = '';    
    error_msg_signup.textContent = '';    
    btn_authin.classList.add('btn-disabled');
    btn_authout.classList.add('btn-disabled');
    btn_signup.classList.add('btn-disabled');



    try {
        if (!auth.currentUser) {
            await signInWithEmailAndPassword(auth, email_field.value, password_field.value);
        } else {
            await signOut(auth);
        }
    } catch (e) {
        switch (e.code) {
            case 'auth/user-not-found':
                error_msg.textContent = 'User not Found';
                break;
            case 'auth/wrong-password':
                error_msg.textContent = 'Wrong password';
                break;
            default:
                error_msg.textContent = 'Something went wrong';
                console.log(e.code);
                console.log(e);
                break;
        }

    } finally {
        btn_authin.classList.remove('btn-disabled');
        btn_authout.classList.remove('btn-disabled');
        btn_signup.classList.remove('btn-disabled');
    }

}

singup_form.addEventListener('submit', async function(e){
    e.preventDefault();
    error_msg_signup.textContent = '';    
    btn_authin.classList.add('btn-disabled');
    btn_authout.classList.add('btn-disabled');
    btn_signup.classList.add('btn-disabled');  
    signup_display_name

    try {
        if (!auth.currentUser) {
            let user = await createUserWithEmailAndPassword(auth, signup_email_field.value, signup_password_field.value);
            user = user.currentUser;
            await user.updateProfile({
                displayName: displayName.value,
            })            
        } else {
            await signOut(auth);
        }
    } catch (e) {
        switch (e.code) {
            // case 'auth/user-not-found':
            //     error_msg.textContent = 'User not Found';
            //     break;
            // case 'auth/wrong-password':
            //     error_msg.textContent = 'Wrong password';
            //     break;
            default:
                error_msg.textContent = 'Something went wrong';
                console.log(e.code);
                console.log(e);
                break;
        }

    } finally {
        btn_authin.classList.remove('btn-disabled');
        btn_authout.classList.remove('btn-disabled');
        btn_signup.classList.remove('btn-disabled');
    }
});