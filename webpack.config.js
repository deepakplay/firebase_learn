const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

const isProduction = process.env.NODE_ENV == "production";

const config = {
   entry: path.resolve(__dirname, 'src', 'index.js'),
   output: {
      path: path.resolve(__dirname, "dist"),
   },
   devServer: {
      open: true,
      host: "localhost",
   },
   plugins: [
      new HtmlWebpackPlugin({
         template: "index.html",
      }),
      new MiniCssExtractPlugin(),
   ],
   module: {
      rules: [
         {
            test: /\.(js|jsx)$/i,
            loader: "babel-loader",
            options: {
               presets: ['@babel/preset-env'],
               plugins: ['@babel/plugin-proposal-object-rest-spread']
            }
         },
         {
            test: /.s?css$/,
            use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "sass-loader"],
         },
         {
            test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
            type: "asset",
         },
      ],
   },

   optimization: {
      minimizer: [
         `...`,
         new CssMinimizerPlugin(),
      ],
      minimize: true,
   }
};

module.exports = () => {
   if (isProduction) {
      config.mode = "production";
   } else {
      config.mode = "development";
   }
   return config;
};
